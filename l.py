#!/usr/bin/python
import lyrics
import lrc
import time
import json
import ast
import os

dl=("Metallica", "Lords of summer")
words=lyrics.MiniLyrics(dl[0], dl[1])
print words
if words==None:
	print "No lyrics found."
	exit()


url=lyrics.get_best_match(words, dl, ".lrc")
#lyrics.download_lyrics(url, dl[1]+".lrc")
#lyrics_str = open(dl[1]+".lrc", 'r').read()

print url
exit(0)

attr, lyric_lines = lrc.parse_lrc(lyrics_str)
print lyric_lines

start=time.time()*1000
pos_ms=0
tot_ms=(3*60000)+(49*1000)
#print tot_ms
lyr_lines=len(lyric_lines)
#print lyr_lines
current_line=0
os.system('clear')
stat=""

def get_next_ts(lyric_lines,ts):
	bn=0
	for line in lyric_lines:
		if bn==1:
			return line['timestamp']
		if line['timestamp']==ts:
			bn=1

while (1):
	nowtime=time.time()*1000
	pos_ms=nowtime-start
	current_line=0
	for line in lyric_lines:
		perc=""
		line_time=int(line['timestamp'])
		current_line=current_line+1		
		next_ts=get_next_ts(lyric_lines,line['timestamp'])
		
		if next_ts and pos_ms>(next_ts-250):
			stat="old"
			
		elif pos_ms<line_time:
			stat="future"

		elif pos_ms>line_time-250:
				stat="next"
#				print line['text']
				perc=str(float(current_line)/lyr_lines*100) + "%"
		
		print stat + " ("+perc+"): " + str(pos_ms) + "/" + str(line_time) + "   " + line['text']
		
	time.sleep(1)
	os.system('clear')
	

