#!/usr/bin/python

import pylast
import time
import errno
import urllib
import socket
import os
import sys
import random
import lyrics
import lrc as lrc_mod

import anydbm

try:
    import xbmc
    import xbmcgui
    import xbmcaddon
    no_kodi=False
except:
    no_kodi=True
    
variables={}

def log(logline):
    print "MUSIC: " + logline

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)

    if no_kodi==True:
        variables[property]=strvalue
    else:
        xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
            db = anydbm.open('music','c')
            try:
                value=db[property]
            except:
                value=""
            db.close()
            
    else:
        value=xbmcgui.Window(10000).getProperty(property)
        
    log("Loading: '" + property + "', '" + value + "'")
    return value
    
#### Go to last.fm and get a key and API secret and put them here!

API_KEY = "e7aa05059d6757471ccbdf7755bc794e" # KEY
API_SECRET = "c54e5e407d8b1e1dd91e81716a100cfb" # SECRET


SLEEP=2

try:
    __addon__       = xbmcaddon.Addon(id='script.service.albumart')
    __addondir__    = xbmc.translatePath( __addon__.getAddonInfo('profile') )
except:
    __addondir__ = "/home/pi/.kodi/userdata/addon_data/script.service.albumart"
    
# There are three things this is downloading.
# Arist art
# Album art
# Song Lyrics

# structured as such:
# __addondir__
# /artist/artist_art.[ext]
# /artist/albumname.[ext]
# /artist/songtitle.[lrc|txt]

# get current artwork into properties. This way the bluetooth handler
# doesn't have to open files. It doesn't like that.
# we can also check the properties there for the existence of album/artist art


def populate_existing():
    walk_dir=__addondir__    
    for root, subdirs, files in os.walk(walk_dir):
        for filename in files:
            file_path = os.path.join(root, filename)
            file_array=file_path.split("/")
            set_kodi_prop(file_array[-3]+"/"+file_array[-2],file_path)

# Testing

#set_kodi_prop("player_artist", "Anthrax")
#set_kodi_prop("player_album", "For All Kings")
#set_kodi_prop("player_title", "Zero Tolerance")

def set_track_properties():
    # Get them
    artist=get_kodi_prop("player_artist")
    album=get_kodi_prop("player_album")
    song=get_kodi_prop("player_title")
    # Now make copies with undersscored in case of slashes
    artist_fix=artist.replace("/","_")
    album_fix=album.replace("/", "_")
    song_fix=song.replace("/", "_")

    artist_exist=0
    album_exist=0
    lyric_exist=0
    
    artist_dir=__addondir__+"/"+artist_fix
    
    # Make the directory
    try:
        os.makedirs(artist_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise  # raises the error again    
    
    # check for artist image, not sure which type it's going to be
    png=os.path.exists(artist_dir+"/artist_art.png")
    jpg=os.path.exists(artist_dir+"/artist_art.jpg")
    if png or jpg:
        log("Artist file exists. ("+artist+")")
        # already exists, do not need to set it
        artist_exist=1
        set_kodi_prop("current_artist_img", artist_dir+"/artist_art."+ ("png" if png else "jpg"))
    
    # check the album image
    png=os.path.exists(artist_dir+"/"+album_fix+".png")
    jpg=os.path.exists(artist_dir+"/"+album_fix+".jpg")
    if png or jpg:
        # already exists, do not need to set it
        album_exist=1
        log("Album file exists. ("+album+")")
        set_kodi_prop("current_album_img", artist_dir+"/"+album_fix+(".png" if png else ".jpg"))

    # check the lyrics now
    lrc=os.path.exists(artist_dir+"/"+song_fix+".lrc")
    txt=os.path.exists(artist_dir+"/"+song_fix+".txt")
    if lrc or txt:
        log("Lyric file exists. ("+song+")")        
        # already exists, do not need to set it
        try:
            lyric_exist=1            
            lyric_txt=open(artist_dir+"/"+song_fix+(".lrc" if lrc else ".txt")).read()
            attr, parsed_lyrics = lrc_mod.parse_lrc(lyric_txt)
            if not parsed_lyrics:
                set_kodi_prop("current_song_txt", lyric_txt)
            else:
                set_kodi_prop("current_song_txt", str(parsed_lyrics))
        except:
            # I'll need to deal with this.. we said the file existed but we failed to open it 
            raise


    # if one of these does not exist. Since we get both from last FM, we'll combine the network calls
    if 0 in [artist_exist, album_exist]:
        network = pylast.LastFMNetwork(api_key = API_KEY, api_secret =  API_SECRET)
        artist_lfm = network.get_artist(artist)
        album_lfm  = network.get_album(artist, album)
        if not artist_exist:
            try:
                artist_img=artist_lfm.get_cover_image()
                log("Downloading artist: " + artist_img)
                url_file=artist_img.split("/")[-1]
                ext=url_file.split(".")[1]
                urllib.urlretrieve(artist_img, artist_dir+"/artist_art."+ext)
                set_kodi_prop("current_artist_img", artist_dir+"/artist_art."+ext)
            except:
                set_kodi_prop("current_artist_img")
                log("Could not download/find image.")
        if not album_exist:
            try:
                album_img=album_lfm.get_cover_image()
                log("Downloading album: " + album_img)
                url_file=album_img.split("/")[-1]
                ext=url_file.split(".")[1]
                urllib.urlretrieve(album_img, artist_dir+"/"+album_fix+"."+ext)
                set_kodi_prop("current_album_img",  artist_dir+"/"+album_fix+"."+ext)
            except:
                set_kodi_prop("current_album_img", "")
                log("Could not download/find image.")                
            
    if not lyric_exist:
        try:
            track=(artist,song)
            lyrics_list=lyrics.MiniLyrics(track[0], track[1])
            lrc=lyrics.get_best_match(lyrics_list, track, ".lrc")
            txt=lyrics.get_best_match(lyrics_list, track, ".txt")
            if not lrc and not txt:
                set_kodi_prop("current_song_txt", "")
            else:
                ext=".lrc" if lrc else ".txt"
                log("Downloading lyrcs: " + (lrc if lrc else txt))
                lyric_file=track[1].replace("/", "_")+ext
                print lyric_file
                lyrics.download_lyrics(lrc if lrc else txt,artist_dir+"/"+song+ext)
                lyric_txt=open(artist_dir+"/"+song+ext).read()
                attr, parsed_lyrics = lrc_mod.parse_lrc(lyric_txt)
                if not parsed_lyrics:
                    set_kodi_prop("current_song_txt", lyric_txt)
                else:
                    set_kodi_prop("current_song_txt", str(parsed_lyrics))
        except:
            set_kodi_prop("current_song_txt", "")
            log("No lyrics found. exception.")
        
    return True

# remove junk that causes the artist from not being looked up
def clean_track_properties():
    badtext=["[Explicit]","(Explicit)","(Box Set)","(Deluxe Edition)"]
    album=get_kodi_prop("player_album")
    song=song=get_kodi_prop("player_title")
    
    for t in badtext:
        album=album.replace(t,"").rstrip()
        song=song.replace(t,"").rstrip()
    
    set_kodi_prop("player_title", song)
    set_kodi_prop("player_album", album)
   
    
if __name__ == '__main__':
    
    log("Starting up")

    artist= ""
    album = ""
    song  = ""

    if no_kodi==False:
        monitor = xbmc.Monitor()

    while True:

        if no_kodi==False:
            if monitor.waitForAbort(1):
                log("ENDING.")
                os._exit(0)

        if get_kodi_prop("player_artist")!="No data":
            if song!=get_kodi_prop("player_title") or \
            	artist!=get_kodi_prop("player_artist") or \
                album!=get_kodi_prop("player_album"):
                    log("Propertie have changed.")
                    clean_track_properties()
                    if set_track_properties():
                        song=get_kodi_prop("player_title")
                        artist=get_kodi_prop("player_artist")
                        album=get_kodi_prop("player_album")
        else:
            set_kodi_prop("current_album_img", "")
            set_kodi_prop("current_artist_img", "")
            set_kodi_prop("current_song_txt", "")
            
        time.sleep(SLEEP)
            
